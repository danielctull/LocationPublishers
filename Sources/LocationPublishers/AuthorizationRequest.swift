
import Combine
import CoreLocation

extension CLLocationManager {

    public func requestAuthorization(for kind: AuthorizationRequest.Kind) -> some Publisher {
        AuthorizationRequest(manager: self, kind: kind)
    }
}

public struct AuthorizationRequest {
    fileprivate let manager: CLLocationManager
    fileprivate let kind: Kind
}

extension AuthorizationRequest {

    public enum Kind {
        case whenInUse
        case always
    }

    public struct Error: Swift.Error {
        public let status: CLAuthorizationStatus
    }
}

extension AuthorizationRequest: Publisher {

    public typealias Output = Void
    public typealias Failure = Error

    public func receive<Subscriber>(subscriber: Subscriber)
        where
        Subscriber: Combine.Subscriber,
        Subscriber.Failure == Failure,
        Subscriber.Input == Output
    {
        let subscription = Subscription(subscriber: subscriber,
                                        manager: manager,
                                        kind: kind)
        subscriber.receive(subscription: subscription)
    }
}

extension AuthorizationRequest {

    fileprivate final class Subscription<Subscriber>: NSObject, CLLocationManagerDelegate
        where
        Subscriber: Combine.Subscriber,
        Subscriber.Failure == Failure,
        Subscriber.Input == Output
    {
        private let subscriber: Subscriber
        private let manager: CLLocationManager
        private let kind: Kind

        init(subscriber: Subscriber,
             manager: CLLocationManager,
             kind: Kind) {
            self.subscriber = subscriber
            self.manager = manager.copy() as! CLLocationManager
            self.kind = kind
            super.init()
            self.manager.delegate = self
        }

        func locationManager(
            _ manager: CLLocationManager,
            didChangeAuthorization status: CLAuthorizationStatus) {

            handleStatus(status)
        }
    }
}

extension AuthorizationRequest.Subscription: Subscription {

    private func handleStatus(_ status: CLAuthorizationStatus) {

        switch (kind, status) {
        case (.always, .authorizedAlways),
             (.whenInUse, .authorizedWhenInUse),
             (.whenInUse, .authorizedAlways):
            _ = subscriber.receive(())
            subscriber.receive(completion: .finished)

        default:
            let error = AuthorizationRequest.Error(status: status)
            subscriber.receive(completion: .failure(error))
        }
    }

    func request(_ demand: Subscribers.Demand) {

        let currentStatus = CLLocationManager.authorizationStatus()
        guard currentStatus == .notDetermined else {
            handleStatus(currentStatus)
            return
        }

        switch kind {
        case .always: manager.requestAlwaysAuthorization()
        case .whenInUse: manager.requestWhenInUseAuthorization()
        }
    }

    func cancel() {}
}
