
import CoreLocation
import Combine

extension CLLocationManager {

    func locationPublisher() -> some Publisher {
        LocationPublisher(manager: self)
    }
}

struct LocationPublisher {
    fileprivate let manager: CLLocationManager
}

extension LocationPublisher: Publisher {

    typealias Output = CLLocation
    typealias Failure = Error

    func receive<Subscriber>(subscriber: Subscriber)
        where
        Subscriber: Combine.Subscriber,
        Subscriber.Failure == Failure,
        Subscriber.Input == Output
    {
        let subscription = Subscription(subscriber: subscriber,
                                        manager: manager)
        subscriber.receive(subscription: subscription)
    }
}

extension LocationPublisher {

    fileprivate final class Subscription<Subscriber>: NSObject, CLLocationManagerDelegate
        where
        Subscriber: Combine.Subscriber,
        Subscriber.Failure == Failure,
        Subscriber.Input == Output
    {
        private let subscriber: Subscriber
        private let manager: CLLocationManager

        init(subscriber: Subscriber,
             manager: CLLocationManager) {
            self.subscriber = subscriber
            self.manager = manager.copy() as! CLLocationManager
            super.init()
            self.manager.delegate = self
        }

        func locationManager(_ manager: CLLocationManager,
                             didUpdateLocations locations: [CLLocation]) {

            locations.forEach { _ = subscriber.receive($0) }
        }
    }
}

extension LocationPublisher.Subscription: Subscription {

    func request(_ demand: Subscribers.Demand) {
        switch demand {
        case .none: return
        case .max(1): manager.requestLocation()
        default: manager.startUpdatingLocation()
        }
    }

    func cancel() {
        manager.stopUpdatingLocation()
    }
}
