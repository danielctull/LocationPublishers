// swift-tools-version:5.1

import PackageDescription

let package = Package(
    name: "LocationPublishers",
    platforms: [
        .iOS(.v13),
        .watchOS(.v6)
    ],
    products: [
        .library(name: "LocationPublishers", targets: ["LocationPublishers"]),
    ],
    targets: [
        .target(name: "LocationPublishers"),
        .testTarget(name: "LocationPublishersTests", dependencies: ["LocationPublishers"]),
    ]
)
